# Flutter Installations And Configurations

## Prerequisites

> Installed: Visual Studio Code ([Download Visual Studio Code](https://code.visualstudio.com/Download))

## Flutter ([Install](https://flutter.dev/docs/get-started/install/macos) And [Set Up An Editor](https://flutter.dev/docs/get-started/editor?tab=vscode))

### macOS Safari Application (Flutter)

> Navigate: Get Started / macOS ([Install](https://flutter.dev/docs/get-started/install/macos))
>
> Select: flutter_macos_[Version]-stable.zip

### macOS Finder Application (Flutter)

> Double Click: flutter_macos_[Version]-stable.zip
>
> Drag And Drop: [flutter To Development Folder]
>
> Select: Continue
>
> Enter: [Password]

### macOS Terminal Application (Flutter Part 1)

`nano ~/.zshenv`

> Enter: export PATH="$PATH:[Development Folder]/flutter/bin"
>
> Enter: Control + x
>
> Enter: y

### macOS App Store Application (Flutter)

> Search: Xcode
>
> Select: Get

### macOS Terminal Application (Flutter Part 2)

`sudo gem install cocoapods`

> Enter: [Password]

`flutter channel dev`

`flutter upgrade`

`flutter config --no-analytics`

`flutter config --enable-macos-desktop`

`flutter config --enable-web`

`flutter doctor`

### Visual Studio Code Application (Flutter)

> Select: [Extensions]
>
> Search: Flutter (dart-code.flutter)
>
> Select: Install
>
> Select: Workspace.code-workspace
>
> Enter: ([Workspace.code-workspace](https://gitlab.com/Ezohr-Public-InstallationsAndConfigurations/FlutterInstallationsAndConfigurations/-/blob/master/Workspace.code-workspace))

```JSON
{
  "folders": [
    {
      "path": "."
    }
  ],
  "settings": {
    "dart.allowAnalytics": false,
    "dart.analysisExcludedFolders": [
      "flutter"
    ]
  }
}
```

## Xcode ([Install](https://flutter.dev/docs/get-started/install/macos))

### macOS Launchpad Application (Xcode)

> Select: Xcode
>
> Select: Agree

### macOS Terminal Application (Xcode Part 1)

`sudo xcode-select --switch /Applications/Xcode.app/Contents/Developer`

`sudo xcodebuild -runFirstLaunch`

### macOS Terminal Application (Xcode Part 2)

`open -a Simulator`

### Simulator Application (Xcode)

> Close: [Simulated Device]
>
> Select: File
>
> Select: Open Device
>
> Select: iOS [Version]
>
> Select: iPhone 11 Pro Max

## Projects ([Test Drive](https://flutter.dev/docs/get-started/test-drive?tab=vscode))

### Visual Studio Code Application (Projects Part 1)

> Edit: .gitignore
>
> Enter: ([.gitignore](https://gitlab.com/Ezohr-Public-InstallationsAndConfigurations/FlutterInstallationsAndConfigurations/-/blob/master/.gitignore))

```sh
# Ignore Android, iOS, macOS and Web Folders
**/android/
**/ios/
**/macos/
**/web/

# Ignore pubspec.lock
pubspec.lock
```

### Visual Studio Code Application (Projects Part 2)

> Select: Run
>
> Select: Start Debugging
>
> Select: Open [Dart Dev Tools]

## Advanced Configurations (General) ([VS Code Extensions Every Flutter Developer Should Have Plus Bonus Theme And Fonts](https://flutter.dev/docs/get-started/test-drive?tab=vscode))

### Visual Studio Code Application (Advanced Configurations (General))

> Navigate: Code / Preferences / Settings
>
> Search: Flutter Ui Guides
>
> Select: Dart: Preview Flutter Ui Guides
>
> Select: Dart: Preview Flutter Ui Guides Custom Tracking
>
> Select: [Extensions]
>
> Search: Tabout (Albert Romkes)
>
> Select: Install
>
> Search: Pubspec Assist (Jeroen Meijer)
>
> Select: Install
>
> Search: Awesome Flutter Snippets (Neevash Ramdial)
>
> Select: Install
>
> Search: Advanced New File (Dominik Kundel)
>
> Select: Install
>
> Search: Bracket Pair Colorizer 2 (CoenraadS)
>
> Select: Install
>
> Search: bloc (Felix Angelov)
>
> Select: Install
>
> Search: Todo Tree (Gruntfuggly)
>
> Select: Install
>
> Search: Better Comments (Aaron Bond)
>
> Select: Install
>
> Select: Install
>
> Search: Material Theme (Mattia Astorino)
>
> Select: Install
>
> Select: Set Colour Theme
>
> Select: Material Theme Darker
>
> Navigate: Code / Preferences / Settings
>
> Search: [Open Settings (JSON)]
>
> Enter: ([settings.json](https://gitlab.com/Ezohr-Public-InstallationsAndConfigurations/FlutterInstallationsAndConfigurations/-/blob/master/Library/Application%20Support/Code/User/settings.json))

```JSON
"workbench.colorCustomizations": {
  "[Material Theme Darker]": {
    "statusBar.debuggingBackground": "#212121"
  }
}
```

> Select: [Extensions]
>
> Search: Material Icon Theme (Philipp Kief)
>
> Select: Install
>
> Navigate: Code / Preferences / Settings
>
> Search: [Open Settings (JSON)]
>
> Enter: ([settings.json](https://gitlab.com/Ezohr-Public-InstallationsAndConfigurations/FlutterInstallationsAndConfigurations/-/blob/master/Library/Application%20Support/Code/User/settings.json))

```JSON
"material-icon-theme.folders.associations": {
  "global_state": "global",
  "user_interface": "layout",
  "bloc": "controller"
}
```

## Advanced Configurations (Font Options) ([VS Code Extensions Every Flutter Developer Should Have Plus Bonus Theme And Fonts](https://flutter.dev/docs/get-started/test-drive?tab=vscode))

### Visual Studio Code Application (Advanced Configurations (Font Options))

> Navigate: Code / Preferences / Settings
>
> Search: Font Ligatures
>
> Select: Edit In settings.json
>
> Enter: ([settings.json](https://gitlab.com/Ezohr-Public-InstallationsAndConfigurations/FlutterInstallationsAndConfigurations/-/blob/master/Library/Application%20Support/Code/User/settings.json))
>

```JSON
"editor.fontLigatures": true
```

### macOS Safari Application (Advanced Configurations (Font Options (1)))

> Enter: <https://github.com/tonsky/FiraCode>
>
> Select: Fira_Code_[Version].zip

### macOS Finder Application (Advanced Configurations (Font Options (1)))

> Double Click: Fira_Code_[Version].zip
>
> Select: Fira_Code_[Version] / ttf
>
> Select: [All]
>
> Select: Open With / Font Book

### macOS Font Book Application (Advanced Configurations (Font Options (1)))

> Select: Install Font

### Visual Studio Code Application (Advanced Configurations (Font Options (1)))

> Navigate: Code / Preferences / Settings
>
> Search: Font Family
>
> Enter: Fira Code (Original "Menlo, Monaco, 'Courier New', monospace")

### macOS Safari Application (Advanced Configurations (Font Options (2)))

> Enter: <https://rubjo.github.io/victor-mono/>
>
> Select: Download
>
> Select: Download
>
> Select: Allow

### macOS Finder Application (Advanced Configurations (Font Options (2)))

> Double Click: VictorMonoAll.zip
>
> Select: VictorMonoAll / ttf
>
> Select: [All]
>
> Select: Open With / Font Book

### macOS Font Book Application (Advanced Configurations (Font Options (2)))

> Select: Install Font

### Visual Studio Code Application (Advanced Configurations (Font Options (2)))

> Navigate: Code / Preferences / Settings
>
> Search: Font Family
>
> Enter: Victor Mono (Original "Menlo, Monaco, 'Courier New', monospace")
>
> Search: Font Weight
>
> Select: 900

## Advanced Configurations (Testing) ([Clean Architecture Plus Test Driven Design](https://youtu.be/lPkWX8xFthE))

### Visual Studio Code Application (Advanced Configurations (Testing))

> Navigate: [Run]
>
> Select: Create A launch.json File
>
> Select: [Workspace]
>
> Select: [Add Configuration...]
>
> Select: Date: Run All Tests
